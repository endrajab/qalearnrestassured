package com.qa.learn.restassured.controller;

import io.restassured.RestAssured;
import io.restassured.config.EncoderConfig;
import io.restassured.config.RestAssuredConfig;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import net.serenitybdd.rest.SerenityRest;

import static io.restassured.config.RestAssuredConfig.config;

public class HttpClient {

    private String requestBody;

    private String endpoint;

    public void setRequestBody(String requestBody){
        this.requestBody = requestBody;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public HttpClient (String endpoint) {
        this.endpoint = endpoint;
    }

    public RequestSpecification BuildRequest() {
        RestAssured.baseURI = "https://sb-mybb-gw.bluebirdgroup.com:8443";
        RequestSpecification request = SerenityRest.given();

        request.config(config().encoderConfig(EncoderConfig.encoderConfig().appendDefaultContentCharsetToContentTypeIfUndefined(false))).
                header("Content-type", "application/json").
                header("App-Version", "4.7.2").
                contentType(ContentType.JSON);
        return request;
    }

    public Response PostMethod(RequestSpecification request) {
        request.body(this.requestBody);
        return request.post(this.endpoint);
    }
}
