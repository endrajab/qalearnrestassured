package com.qa.learn.restassured.steps;

import com.qa.learn.restassured.controller.HttpClient;
import com.qa.learn.restassured.model.RequestLoginModel;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.Assert;
import org.openqa.selenium.json.Json;

public class LoginSteps {

    String body;

    Response response;

    RequestLoginModel requestLoginModel = new RequestLoginModel();

    @Given("^email is (.*) and password is (.*)$")
    public void setEmailPassword(String email, String password){
        requestLoginModel.setEmail(email);
        requestLoginModel.setPassword(password);
        requestLoginModel.setUuid("asdf-asdf-asdf");
        requestLoginModel.setOperating_system("ios");
        requestLoginModel.setManufacture("Apple");
        requestLoginModel.setOs_version("13.0");
        requestLoginModel.setDevice_token("poiuyt");
    }

    @When("^user access \"([^\"]*)\" for login endpoint$")
    public void setEndpoint(String endpoint){
        HttpClient httpClient = new HttpClient(endpoint);

//        Json json = new Json();
//        body = json.toJson(requestLoginModel);

        httpClient.setRequestBody(requestLoginModel);

        RequestSpecification request = httpClient.BuildRequest().log().all();

        response = httpClient.PostMethod(request.log().all());
    }

    @Then("^user get the status code (.*)$")
    public void validateStatusCode(int statusCode) {
        Assert.assertEquals(response.getStatusCode(), statusCode);
    }
}
